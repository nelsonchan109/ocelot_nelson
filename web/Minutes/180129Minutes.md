Agenda
=======
1. To update registration form input function
2. To implement profile page dynamic
3. Admin page design & implementation
4. Modify user design & implementation


Minutes/Notes
=======
1.Successfully updated registration form input
- Every group member involved in the form re-design and update.

2. Finish 50% of profile page dynamic display information from database
- Every group member involved in the process of design and coding of create article page layout.

3. Admin page design & implementation
- Every group member involved in design and coding of servlet.

4. Modify user design & implementation
- Every group member involved in design and coding of servlet.


Next Steps:
=======
1. To retrieve comments from database for personal page and implement dynamic nested comments.
2. To successfully implement log out function for personal page. 
3. Link create article function and save into database.
4. Send email to user & ask them to resent password.
