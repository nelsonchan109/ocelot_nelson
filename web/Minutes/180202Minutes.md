Agenda
=======
1. To implement remove user function on admin page.
2. To implement search article function on main page and user page.

Minutes/Notes
=======
1.Successfully implemented admin page function 
- Every group member involved in the coding and implementation of admin page

2.Successfully implemented remove user function on admin page 
- Every group member involved in the coding and implementation of removing user function on admin page

Next Steps:
=======
1. Modify navigation bar to look consistent. 
2. Modify bug for summernote.
