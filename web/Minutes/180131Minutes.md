Agenda
=======
1. To link registration page button to other pages.
2. To beautify registration form 
3. To implement admin function

Minutes/Notes
=======
1.Successfully linked registration page button to other pages 
- Every group members involved in coding of linking registration page button to other pages

2.Successfully making registration form more presentable
- Every group members involved in design of making registration form more presentable

Next Steps:
=======
1. To implement admin function
2. To successfully implement upload picture function 
