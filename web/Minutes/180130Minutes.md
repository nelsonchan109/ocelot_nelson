Agenda
=======
1. To create article and save into database.
2. To update registration form input. 

Minutes/Notes
=======
1.Successfully create article and save articles into database
- Every group members involved in function of create article and coding of saving articles into database. 

2.Successfully update registration form input
- Every group members involved in updating the registration form and linking it to database. 

Next Steps:
=======
1. To link registration page button to other pages
2. To beautify registration form 
3. To implement admin function
