Agenda
=======
1. Completion of main page design (All participants involved)
2. Compilation of main page and log in page(All participants involved)
3. Completion of personal page design 
4. Design and coding of servlet classes

Minutes/Notes
=======
1.Completion of main page design (All participants involved)
- Discussion of main page design and improvement ( All participants involved)

2.Compilation of main page and log in page(All participants involved)
- Every group member involved in compilation of main page and log in page
- reCAPTCHA design completed (Nelson)

3.Completion of personal page design 
- Every group member involved in compilation of personal page design 
- WYSIWYG being created and discussed among members 

4.Design and coding of servlet classes 
- All group members are involved in designing and coding of servlet 

Next Steps:
=======
1. To discuss about retrieving data from database and include the data in our main page and personal page
2. To discuss about linking function to button and navigation bars of main page and personal page.
3. To discuss about how to implement nested comment.
