

function currentUnixTime() {
    var d = new Date();
    return d.getTime();
}


function assertNotNull(obj) {
    if (obj == null
        || obj == undefined) {
        alert("invalid object");
    }
}

function assertNotEmptyString(obj) {
    if (obj == null
        || obj == undefined
        || obj.length === 0) {
        alert("invalid object");
    }
}

