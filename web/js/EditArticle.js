var edit = function() {
    var article_id = $(this).attr("article_id");
    $("#articleID" + article_id)[0].className += "click2edit";

    $("#title"+article_id).hide();

    var titleLabel  = $("<label>Title</label>");
    titleLabel.attr("class", "control-label");
    titleLabel.attr("id", "newTitleLbl" + article_id);

    var titleInput = $("<input>");
    titleInput.attr("type", "text");
    titleInput.attr("id", "newTitle" + article_id);
    titleInput.attr("placeholder", "change your title here");
    titleInput.attr("class", "form-control");
    titleInput.attr("name", "title");


    $("#titleDiv" + article_id).append(titleLabel, titleInput);



    $('.click2edit').summernote({focus: true,
        toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
    ],
        placeholder: 'Please type your content here',
        tabsize: 2,
        height: 200,
        width: 600
    });

    document.getElementById("articleID" + article_id).className = document.getElementById("articleID" + article_id).className.replace(/\bclick2edit\b/,'');



};

var save = function() {
    var article_id = $(this).attr("article_id");
    $("#articleID" + article_id)[0].className += "click2edit";


    var markup = $('.click2edit').summernote('code');

    $('.click2edit').summernote('destroy');
    document.getElementById("articleID" + article_id).className = document.getElementById("articleID" + article_id).className.replace(/\bclick2edit\b/,'');


    var newTitle;

    newTitle = $("#newTitle" + article_id).val();

    if (newTitle === ""){
        newTitle = "No Title";
    }

    $("#title"+article_id).text(newTitle);
    $("#title"+article_id).show();
    $("#newTitle" + article_id).hide();
    $("#newTitleLbl" +article_id).hide();

    var content = $("#articleID"+article_id).html();

    modifyArticleById(article_id, newTitle,  content,

        function (data) {
            alert("modify Article success");
            $("#newTitle" + article_id).remove();
            $("#newTitleLbl" +article_id).remove();
        },

        function (data) {
            alert("modify Article failed" + JSON.stringify(data));
            $("#newTitle" + article_id).remove();
            $("#newTitleLbl" +article_id).remove();
        }

    );

};



var deleteFn = function () {
    var article_id = $(this).attr("article_id");

    deleteArticleById(article_id,
        function (data) {
            alert("remove Article success");
            window.location.replace("Profile.jsp");
        },

        function (data) {
            alert("remove Articlefailed" + JSON.stringify(data));
        }

    );

};



$(document).ready(function () {

    setTimeout(function () {
        $('body').addClass('loaded');
        $('h1').css('color', '#222222');
    }, 2000);


});



