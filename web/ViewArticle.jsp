<%--
  Created by IntelliJ IDEA.
  User: sc355
  Date: 3/02/2018
  Time: 11:35 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="org.jooq.util.derby.sys.Sys" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>View Article</title>
    <link rel="icon" type="image/jpg" href="Photos/ocelot.jpg" />

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
          crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- JavaScript -->
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/bootstrap.min.css"/>

    <!-- include summernote css/js -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

    <script type="text/javascript" src="js/Helper.js"></script>
    <script type="text/javascript" src="js/ajax/Article.js"></script>
    <script type="text/javascript" src="js/ajax/User.js"></script>
    <script type="text/javascript" src="js/ajax/Comment.js"></script>
    <link href="Login.css" rel="stylesheet" type="text/css">

    <!-- include Nelson js -->
    <script type="text/javascript" src="js/EditArticle.js"></script>


    <!-- Collect the nav links, forms, and other content for toggling -->
    <nav class="navbar navbar-inverse">
        <ul class="nav navbar-nav">
            <li><a href="Menu.jsp"><span class="glyphicon glyphicon-home"></span>MainPage</a></li>
            <li id="myPage"><a href="Profile.jsp"><span class="glyphicon glyphicon-user"></span>MyPage</a></li>
            <!--<li id="myHistory" class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">MyHistory <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">History</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Comment</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Photos</a></li>
                </ul>
            </li>-->
        </ul>

        <ul class="nav navbar-nav navbar-right">

            <li id="createArticle"><a href="CreateArticle.jsp">CreateArticle<span class="glyphicon glyphicon-file"></span></a></li>

        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li id="editDetails" class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">EditDetails <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="SelfDetail.jsp">ChangeAccountInfo</a></li>
                    <li role="separator" class="divider"></li>
                    <li id="deleteAccount"><a href="#">DeleteAccount</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="Logout">Logout</a></li>
                </ul>
            </li>
        </ul>

        <!--<form class="navbar-form navbar-right">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-default">Search</button>

        </form> -->

    </nav>
</head>
<body>
<div class ="container">



    <div id="userArticles"></div>

</div>

<script>

    function escapeSingleQuote(origin) {

        var result = "";

        for (var i = 0; i < origin.length; i++) {
            if (origin.charAt(i) == "'") {
                result += "\'";
            } else {
                result += origin.charAt(i);
            }
        }

        return result;
    }

    $(document).ready(function () {
        /** Disabled user features when the status is logout **/
        if ("${userSession.userName}" === "") {
            $('#myPage').hide();
            $('#myHistory').hide();
            $('#createArticle').hide();
            $('#editDetails').hide();

        }
    });

        var currentTime = new Date();
        //currentTime.getTime();
        var articlePubtime = new Date(${article.pubtime.getTime()});
        //articlePubtime.getTime();

        /** Create "RECENT POST" title **/
        var recentPostH4 = $("<h4></h4>");
        recentPostH4.append("<small>" + "RECENT POSTS" + "</small>");
        $("#userArticles").append(recentPostH4);
        $("#userArticles").append($("<hr>"));

        /** Create Buttons **/
        var editButton = $("<button></button>");
        editButton.attr("type", "button");
        editButton.attr("class", "btn btn-default btn-sm");
        editButton.on("click", edit);
        editButton.attr("article_id", "${article.id}");
        editButton.attr("id", "editBtn" + "${article.id}");
        editButton.append($("<span class=\"glyphicon glyphicon-edit editButton\"></span>"));
        editButton.append("Edit");
        //$("#userArticles").append(editButton);

        var saveButton = $("<button></button>");
        saveButton.attr("type", "button");
        saveButton.attr("class", "btn btn-default btn-sm");
        saveButton.on("click", save);
        saveButton.attr("article_id", "${article.id}");
        saveButton.attr("id", "saveBtn" + "${article.id}");
        saveButton.append($("<span class=\"glyphicon glyphicon-floppy-disk saveButton\"></span>"));
        saveButton.append("Save");
        //$("#userArticles").append(saveButton);

        var deleteButton = $("<button></button>");
        deleteButton.attr("type", "button");
        deleteButton.attr("class", "btn btn-default btn-sm");
        deleteButton.on("click", deleteFn);
        deleteButton.attr("article_id", "${article.id}");
        deleteButton.attr("id", "deleteBtn" + "${article.id}");
        deleteButton.append($("<span class=\"glyphicon glyphicon-trash deleteButton\"></span>"));
        deleteButton.append("Delete");
        //$("#userArticles").append(deleteButton);

        if ("${userSession.userName}" != "" && "${userSession.userName}" === "${article.userName}" ) {
            $("#userArticles").append(editButton, saveButton, deleteButton);
        }


        /** Create Article Title **/
        var titleDiv = $("<div></div>");
        titleDiv.attr("id", "titleDiv" + "${article.id}");

        var articleTitle = $("<h2></h2>").text("" + "${article.title}" + "");
        articleTitle.attr("id", "title"+"${article.id}");
        titleDiv.append(articleTitle);
        $("#userArticles").append(titleDiv);

        /** Create Post Date & time **/
        var postDateTime = $("<h5></h5>");
        postDateTime.append($("<span class=\"glyphicon glyphicon-time\"></span>"));

        if(articlePubtime <= currentTime){
            postDateTime.append("Posted by " + "${article.userName}" + " " + articlePubtime);
        } else
        {
            postDateTime.append("Reminder: This article will be post on " + articlePubtime);
            postDateTime.attr("style", "color:green");
        }

        $("#userArticles").append(postDateTime);

        /** Create Article Content **/
        var articleContentDiv = $("<div></div>");
        articleContentDiv.attr("id", "articleID"+"${article.id}");

        getArticleById("${article.id}",
            function (data) {
                var article = data["result"];
                    articleContentDiv.append(article.content);


        }, function (data) {
                alertify.error("Error\n" + data.responseText);
            }

        );

        $("#userArticles").append(articleContentDiv);

        $("#userArticles").append("<br><br>");


        /** Create Comment **/
        var CommentDiv = $("<div></div>");
        CommentDiv.attr("id", "commentDiv"+"${article.id}");
        CommentDiv.append("<h4>Leave a Comment:</h4>");
        $("#userArticles").append(CommentDiv);



        comment("${article.id}");

        function comment (ArticleId){
            getCommentTreeByArticleId(ArticleId,

                function (data) {
                    var comments = data["result"];

                    /**Comment Form Div**/
                    var commentForm = $("<form></form>");
                    commentForm.attr("role", "form");
                    commentForm.attr("id", "formId" + ArticleId);

                    var FormDiv = $("<div></div>");
                    FormDiv.attr("class", "form-group");

                    var textAreaForm = $("<textarea></textarea>");
                    textAreaForm.attr("class", "form-control");
                    textAreaForm.attr("row", "3");
                    textAreaForm.attr("id", "commentTextId" + ArticleId);
                    textAreaForm.prop("required", true);

                    var commentButton = $("<button></button>");
                    commentButton.append("Comment");
                    commentButton.attr("type", "button");
                    commentButton.attr("class", "btn btn-success");
                    commentButton.attr("commentArticleId", ArticleId);
                    commentButton.on("click", submitComment);

                    commentForm.append(FormDiv, commentButton);
                    FormDiv.append(textAreaForm, commentButton);

                    var reminderText = $("<p></p>");
                    reminderText.text("Reminder: Please login to leave your comment");
                    reminderText.attr("style", "color:red");

                    if ("${userSession.userName}" === "") {
                        textAreaForm.attr("disabled", "disabled");
                        commentButton.attr("disabled", "disabled");

                        CommentDiv.append(reminderText);

                    }

                    $("#commentDiv"+ArticleId).append(FormDiv);
                    $("#commentDiv"+ArticleId).append("<br><br>");

                    /** Only appear when it have comments**/
                    if (comments.length>0) {
                        var commentP = $("<p></p>");
                        commentP.attr("id", "commentP" + ArticleId);
                        commentP.append("<span class=\"badge\"></span> Comments:");
                        $("#commentDiv" + ArticleId).append(commentP);
                        $("#commentDiv" + ArticleId).append("<br>");
                    }

                    var rowDiv = $("<div></div>");
                    rowDiv.attr("class", "row");

                    for (var i = 0; i < comments.length; ++i) {

                        /** Commentor Pic **/
                        var commentorPicDiv = $("<div></div>");
                        commentorPicDiv.attr("id", "commentPicDiv" + comments[i].id);
                        commentorPicDiv.attr("class", "col-sm-2 text-center");
                        var commentorPic = $("<img>");
                        commentorPic.attr("src", comments[i].user_icon);
                        commentorPic.attr("class", "img-circle");
                        commentorPic.attr("height", "65");
                        commentorPic.attr("width", "65");
                        commentorPic.attr("alt", "Avatar");
                        commentorPic.attr("id", "commentPic" + comments[i].id);

                        rowDiv.append(commentorPicDiv);
                        commentorPicDiv.append(commentorPic);

                        /** Commentor Comment **/
                        var commentorContentDiv = $("<div></div>");
                        commentorContentDiv.attr("id", "commentContentDiv" + comments[i].id);
                        commentorContentDiv.attr("class", "col-sm-10");
                        rowDiv.append(commentorContentDiv);

                        /** Commentor Name **/
                        var commentorH4 = $("<h4></h4>");
                        commentorH4.append(comments[i].user_name + " ");

                        /** Comment Date **/
                        var commentDate = $("<small>"+ new Date(comments[i].pubtime) + "</small>");
                        commentorH4.append(commentDate);
                        commentorContentDiv.append(commentorH4);
                        $("rowDiv").append(commentorContentDiv);

                        /** Comment Content **/
                        var commentContent = $("<p></p>");
                        commentContent.attr("id", "commentContentId" + comments[i].id);
                        commentContent.append(comments[i].content);
                        commentorContentDiv.append(commentContent);

                        /** Comment reply button**/
                        var topLvReplyDiv = $("<div></div>");
                        topLvReplyDiv.attr("id", "replyDivId" + comments[i].id);
                        var topLvReply = $("<button></button>").append("Reply");
                        topLvReply.attr("replyId", comments[i].id);
                        topLvReply.attr("replyArticleId", ArticleId);
                        //topLvReply.attr("class", "glyphicon glyphicon-share-alt btn btn-success");
                        topLvReply.attr("class", "btn btn-primary btn-sm");
                        topLvReply.attr("replyUserId", comments[i].user_id);

                        if ("${userSession.userName}" === "") {
                            topLvReply.attr("disabled", "disabled");
                        }

                        //topLvReply.attr("topParentId", comments[i].id);
                        topLvReply.on("click", showCommentButton);
                        topLvReplyDiv.append(topLvReply);
                        commentorContentDiv.append(topLvReplyDiv);

                        /** Comment delete button**/
                        if (("${userSession.id}") === (comments[i].user_id)){
                            var deleteSpan = $("<span></span>");
                            deleteSpan.attr("id", "deleteSpanId" + comments[i].id);
                            var deleteComment = $("<button></button>").append("Delete");
                            deleteComment.attr("class", "btn btn-basic btn-sm");
                            deleteComment.attr("deleteButtonId", comments[i].id);
                            deleteComment.on("click", deleteCommentFn);
                            deleteSpan.append(deleteComment);
                            topLvReplyDiv.append("&nbsp; &nbsp; &nbsp;");
                            topLvReplyDiv.append(deleteSpan);
                        }

                        commentorContentDiv.append("<br>");

                        $("#commentDiv"+ArticleId).append(rowDiv);

                        /** Nested Comment Here **/
                        var children = comments[i].children;
                        for (var ind = 0; ind < children.length; ind ++ ) {

                            var commentBadge =  $("<p></p>");
                            commentBadge.append("<span class=\"badge\"></span> Comments:");
                            commentorContentDiv.append(commentBadge);

                            var rowDivChild = $("<div></div>");
                            rowDivChild.attr("class", "row");
                            commentorContentDiv.append( rowDivChild);

                            /** Child Commentor Pic **/
                            var commentorChildPicDiv = $("<div></div>");
                            commentorChildPicDiv.attr("id", "commentChildPicDiv" + children[ind].id);
                            commentorChildPicDiv.attr("class", "col-sm-2 text-center");
                            var commentorChildPic = $("<img>");
                            commentorChildPic.attr("src", children[ind].user_icon);
                            commentorChildPic.attr("class", "img-circle");
                            commentorChildPic.attr("height", "65");
                            commentorChildPic.attr("width", "65");
                            commentorChildPic.attr("alt", "Avatar");
                            commentorChildPic.attr("id", "commentPic" + children[ind].id);

                            rowDivChild.append(commentorChildPicDiv);
                            commentorChildPicDiv.append(commentorChildPic);

                            /** Child Commentor Comment **/
                            var commentorChildContentDiv = $("<div></div>");
                            commentorChildContentDiv.attr("id", "commentChildContentDiv" + children[ind].id);
                            commentorChildContentDiv.attr("class", "col-xs-10");
                            rowDivChild.append(commentorChildContentDiv);

                            /** Child Commentor Name **/
                            var commentorChildH4 = $("<h4></h4>");
                            commentorChildH4.append(children[ind].user_name + " ");

                            /** Child Comment Date **/
                            var commentChildDate = $("<small>"+ new Date(children[ind].pubtime) + "</small>");
                            commentorChildH4.append(commentChildDate);
                            commentorChildContentDiv.append(commentorChildH4);
                            rowDivChild.append(commentorChildContentDiv);

                            /** Child Comment Content **/
                            var commentChildContent = $("<p></p>");
                            //commentChildContent.attr("id", "commentChildContentId" + children[i].id);
                            commentChildContent.append(children[ind].content);
                            commentorChildContentDiv.append(commentChildContent);

                            /** Child Comment reply button**/
                            var replyDivChild = $("<div></div>");
                            replyDivChild.attr("id", "replyDivChildId" + children[ind].id);
                            var replyChild = $("<button></button>").append("Reply");
                            replyChild.attr("replyChildId", children[ind].id);
                            replyChild.attr("replyChildArticleId", ArticleId);
                            replyChild.attr("parentId", comments[i].id);
                            replyChild.attr("class", "btn btn-primary btn-sm");

                            if ("${userSession.userName}" === "") {
                                replyChild.attr("disabled", "disabled");
                            }

                            replyChild.attr("replyChildUserId", comments[i].user_id);
                            replyChild.on("click", showLastChildCommentButton);
                            replyDivChild.append(replyChild);
                            commentorChildContentDiv.append(replyDivChild);

                            /** Child Comment delete button**/
                            if (("${userSession.id}") === (children[ind].user_id)){
                                var deleteSpanChild = $("<span></span>");
                                deleteSpanChild.attr("id", "deleteSpanChildId" + children[ind].id);
                                var deleteCommentChild = $("<button></button>").append("Delete");
                                deleteCommentChild.attr("class", "btn btn-basic btn-sm");
                                deleteCommentChild.attr("deleteChildButtonId", children[ind].id);
                                deleteCommentChild.on("click", deleteChildCommentFn);

                                deleteSpanChild.append(deleteCommentChild);
                                replyDivChild.append("&nbsp;");
                                replyDivChild.append(deleteSpanChild);
                            }

                            commentorChildContentDiv.append("<br>");

                        }



                    } /** End of Comment For-loop **/
                },

                function (data) {
                    alertify
                        .alert("Error\n" + data.responseText, function(){
                            alertify.message('OK');
                        }).setHeader('<em> Team Ocelot </em> ');
                }
            );

        }


    //} else{
        //$("#displayArticleDiv").append("<p>no user</p>");


</script>



<script>
    function showCommentButton() {
        var replyCommentId = $(this).attr("replyId");
        var replyArticleId = $(this).attr("replyArticleId");
        var replyUserId = $(this).attr("replyUserId");
        //var replyParentId = $(this).attr("topParentId");



        var space = $("<br>");


        var commentForm = $("<form></form>");
        commentForm.attr("role", "form");

        var FormDiv = $("<div></div>");
        FormDiv.attr("class", "form-group");

        var textAreaForm = $("<textarea></textarea>");
        textAreaForm.attr("class", "form-control");
        textAreaForm.attr("row", "3");
        textAreaForm.attr("id", "replyTextId" + replyCommentId);
        textAreaForm.prop("required", true);

        var commentButton = $("<button></button>");
        commentButton.append("Comment");
        commentButton.attr("type", "button");
        commentButton.attr("class", "btn btn-success");
        commentButton.attr("commentButtonArticleId", replyArticleId);
        commentButton.attr("replyCommentButtonId", replyCommentId);
        commentButton.attr("commentChildArticleId", replyArticleId);
        commentButton.attr("replyCommentUserId",replyUserId);
        commentButton.attr("replyParentId", replyCommentId);
        commentButton.on("click", submitChildComment);

        commentForm.append(FormDiv, commentButton);
        FormDiv.append(space, textAreaForm, commentButton);

        $("#replyDivId" + replyCommentId).append(commentForm);

    }

    function submitComment(){
        var articleId = $(this).attr("commentArticleId");

        var content = $("#commentTextId" + articleId).val();
        console.log(content);

        addComment("${userSession.id}",articleId, null, content,

            function (data) {
                alertify
                    .alert("Add Comment success", function(){
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                window.location.replace("ViewSingleArticleServlet?article_id=" + "${article.id}");
            },

            function (data) {
                alertify
                    .alert("Add Comment failed" + JSON.stringify(data), function(){
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
            }

        );

    }

    function deleteCommentFn(){
        var articleId = $(this).attr("deleteButtonId");

        deleteCommentById(articleId,

            function (data) {
                alertify
                    .alert("Delete Comment success", function(){
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                window.location.replace("ViewSingleArticleServlet?article_id=" + "${article.id}");
            },

            function (data) {
                alertify
                    .alert("Delete Comment failed" + JSON.stringify(data), function(){
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
            });

    }

    function deleteChildCommentFn(){
        var articleId = $(this).attr("deleteChildButtonId");

        deleteCommentById(articleId,

            function (data) {
                alertify
                    .alert("Delete Comment success", function(){
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                window.location.replace("Profile.jsp");
            },

            function (data) {
                alertify
                    .alert("Delete Comment failed" + JSON.stringify(data), function(){
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
            });

    }


    function submitChildComment(){
        var replyArticleId = $(this).attr("commentButtonArticleId");
        var replyId = $(this).attr("replyCommentButtonId"); //commentID
        var replyParentId = $(this).attr("replyParentId");



        var content = $("#replyTextId" + replyId).val();
        console.log(content);

        addComment("${userSession.id}",replyArticleId, replyParentId, content,

            function (data) {
                alertify
                    .alert("Add Comment success", function(){
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                window.location.replace("ViewSingleArticleServlet?article_id=" + "${article.id}");
            },

            function (data) {
                alertify
                    .alert("Add Comment failed" + JSON.stringify(data), function(){
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
            }

        );

    }
    /** for 2nd level comment, but we dont need to do that**/
    function showLastChildCommentButton() {
        var replyChildArticleId = $(this).attr("replyChildArticleId");
        var replyChildCommentId = $(this).attr("replyChildId");
        var replyChildUserId = $(this).attr("replyChildUserId");
        var parentId = $(this).attr("parentId");


        var space = $("<br>");


        var commentForm = $("<form></form>");
        commentForm.attr("role", "form");

        var FormDiv = $("<div></div>");
        FormDiv.attr("class", "form-group");

        var textAreaForm = $("<textarea></textarea>");
        textAreaForm.attr("class", "form-control");
        textAreaForm.attr("row", "3");
        textAreaForm.attr("id", "replyChildTextId" + replyChildCommentId);
        textAreaForm.prop("required", true);

        var commentButton = $("<button></button>");
        commentButton.append("Comment");
        commentButton.attr("type", "button");
        commentButton.attr("class", "btn btn-success");
        commentButton.attr("commentChildButtonArticleId", replyChildArticleId);
        commentButton.attr("replyChildCommentButtonId", replyChildCommentId);
        commentButton.attr("commentLastChildArticleId", replyChildArticleId);
        commentButton.attr("replyChildCommentUserId",replyChildUserId);
        commentButton.attr("topCommentId", parentId);
        commentButton.on("click", submitLastChildComment);

        console.log("showLastChildCommentButton ID: " + parentId);

        commentForm.append(FormDiv, commentButton);
        FormDiv.append(space, textAreaForm, commentButton);

        $("#replyDivChildId" + replyChildCommentId).append(commentForm);
    }

    //TODO
    function submitLastChildComment() {
        var replyChildArticleId = $(this).attr("commentChildButtonArticleId");
        var replyChildId = $(this).attr("replyChildCommentButtonId"); //commentID
        var parentId = $(this).attr("topCommentId");


        var content = $("#replyChildTextId" + replyChildId).val();
        console.log(content);

        addComment("${userSession.id}", replyChildArticleId, parentId, content,

            function (data) {
                alertify
                    .alert("Add Comment success", function(){
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
                window.location.replace("ViewSingleArticleServlet?article_id=" + "${article.id}");
            },

            function (data) {
                alertify
                    .alert("Add Comment failed" + JSON.stringify(data), function(){
                        alertify.message('OK');
                    }).setHeader('<em> Team Ocelot </em> ');
            }
        );
    }



</script>


</body>
</html>
