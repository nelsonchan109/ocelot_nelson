
DROP TABLE IF EXISTS Comment;
DROP TABLE IF EXISTS Article;
DROP TABLE IF EXISTS User;


CREATE TABLE IF NOT EXISTS User (
  id INT AUTO_INCREMENT,
  username VARCHAR(128) NOT NULL UNIQUE ,
  fname VARCHAR(64),
  lname VARCHAR(64),
  passwd VARCHAR(128),
  gender  VARCHAR(16),
  email  VARCHAR(128) NOT NULL UNIQUE ,
  icon   VARCHAR(128) DEFAULT 'Photos/thumbnails/gentlemen.png',
  dob  DATE NOT NULL,
  country VARCHAR(64),
  description TEXT,
  verified INT DEFAULT '0',
  PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS Article (
  id INT AUTO_INCREMENT,
  user_id INT NOT NULL,
  title VARCHAR(256),
  content TEXT,
  pubtime DATETIME DEFAULT CURRENT_TIMESTAMP,
  banned INT DEFAULT '0',
  hitcount INT DEFAULT '0',
  likecount INT DEFAULT '0',
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES User(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Comment (
  id INT AUTO_INCREMENT,
  user_id INT NOT NULL,
  article_id INT NOT NULL,
  parent_id INT,
  content TEXT,
  pubtime DATETIME DEFAULT CURRENT_TIMESTAMP,
  banned INT DEFAULT '0',
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES User(id) ON DELETE CASCADE,
  FOREIGN KEY (article_id) REFERENCES Article(id) ON DELETE CASCADE,
  FOREIGN KEY (parent_id) REFERENCES Comment(id) ON DELETE CASCADE
);

INSERT INTO User(username, fname, lname, passwd, gender, email, dob, country, description) VALUES
  ('admin', 'admin', '', '', 'male', 'admin@gmail.com', '2018-1-25', 'NZ', 'I am in charge here!'),
  ('test', 'test', '', 'test', 'male', 'someone@hotmail.com','2000-1-1', 'China', 'nothing to say'),
  ('demo', 'demo', '', 'demo', 'male', 'someone@gmail.com','2001-10-11', 'New Zealand', 'nothing to say');

INSERT INTO Article(user_id, title, content) VALUES
  ('1','Big Whale', 'Last Night I dreamt that I was riding a big whale. It was so cool.');

INSERT INTO Article(user_id, title, content) VALUES
  ('2','Time Traveller', 'Our article is the best article in the world. (?)');


INSERT INTO Comment(user_id, article_id, content) VALUES
  ('1','1', 'I like whale too, I hope I can ride a whale');


INSERT INTO Article(user_id, title, content) VALUES
  ('2','Ocelot', '<h2>
    <Stong>Characteristics</Stong>
</h2>

<p>
    The ocelot is a medium-sized spotted cat, similar to the bobcat in physical proportions. The ocelot is between 55 and 100 centimetres (22 and 39 in) in head-and-body length and weighs 8–16 kilograms (18–35 lb). Larger individuals have occasionally been recorded. The thin tail, 26–45 centimetres (10–18 in) long, is ringed or striped and is shorter than the hindlimbs. The round ears are marked with a bright white spot, in contrast with the black background. The eyes are brown, and gleam golden when exposed to light.
</p>

<p>The fur is short and smooth; the back is basically creamy, tawny, yellowish, reddish grey or grey, while the neck and underside are white. The guard hairs (the hairs above the basal hairs of the back) are 1 centimetre (0.39 in) long, while the fur on the underbelly measures 0.8 centimetres (0.31 in). The coat is extensively marked with a variety of solid black markings – these vary from open or closed bands and stripes on the back, cheeks and flanks to small spots on the head and limbs. A few dark stripes run straight from the back of the neck up to the tip of the tail. A few horizontal streaks can be seen on the insides of the legs. English naturalist Richard Lydekker observed that the ocelot is "one of the most difficult members of the feline family to describe". In 1929, wildlife author Ernest Thompson Seton described the coat of the ocelot as "the most wonderful tangle of stripes, bars, chains, spots, dots and smudges...which look as though they were put on as the animal ran by." The spoor measures nearly 2 by 2 centimetres (0.79 in × 0.79 in). The ocelot can be easily confused with the margay, but differs in being twice as heavy, having a greater head-and-body length, a shorter tail, smaller eyes relative to the size of the head, and different cranial features. The similar jaguar is notably larger and heavier, and has rosettes instead of spots and stripes.
</p>

<h2>
    Ecology and behavior
</h2>
<p>
The ocelot is active around twilight and at night and hence difficult to observe. However, it can be seen hunting in daytime as well – especially on cloudy or rainy days. The ocelot is active for 12 to 14 hours every day, and hunting is the major activity. It rests mainly during the day and in a variety of places, such as tree branches, depressions at the base of trees or under fallen trees. Nocturnality in ocelots appears to increase in areas where they face significant hunting risk. The ocelot moves 1.8–7.6 kilometres (1.1–4.7 mi) every night, especially on certain favored trails; males appear to roam twice the distance covered by females. Ocelots in Peru were observed resting for a few hours in the midnight after their walk. Ocelots are known to swim efficiently. They can produce a long-range "yowl" in the mating season as well as short-range vocalizations like "meow"s.
</p>

<p>Solitary animals, ocelots live singly in territories that are scent-marked by urine spraying and forming dung piles. Male territories are 3.5–46 square kilometres (1.4–17.8 sq mi) large, while those of females cover 0.8–15 square kilometres (0.31–5.79 sq mi). Ranges of females hardly overlap, whereas the territory of a male can include the territories of two to three females in oestrus. Social interaction is minimal, though a few adults have been observed together even in non-mating months, and some juveniles may interact with their parents. Ocelots also appear in high densities in Peru and Venezuela, where densities can reach 0.4–0.8 per square kilometre (1.0–2.1/sq mi). Barro Colorado Island holds the highest ocelot density recorded: 1.59–1.74 per square kilometre (4.1–4.5/sq mi). This is probably due to higher prey availability, increased protection from poaching and reduced occurrence of large predators. A study suggested that ocelot densities in an area may fall if rainfall decreases.
</p>');

INSERT INTO Article(user_id, title, content) VALUES
  ('3','Waitangi Day', '<h1>
    Waitangi Day
</h1>

<p>
    Waitangi Day, named after Waitangi where the <a href="https://en.wikipedia.org/wiki/Treaty_of_Waitangi">Treaty of Waitangi</a> was first signed, commemorates a significant day in the history of New Zealand. Ceremonies take place each year on 6 February to celebrate the signing of the treaty, New Zealand''s founding document, on that date in 1840. The day is a public holiday, unless it falls on a Saturday or Sunday, when the Monday that immediately follows becomes the public holiday.
</p>


<h2>
    Early celebrations
</h2>
<p>
    Prior to 1934, most celebrations of New Zealand''s founding as a colony were held on 29 January, the date on which William Hobson arrived in the Bay of Islands to issue the proclamation, which had been prepared by colonial office officials in England. Hobson had no draft treaty. From the British perspective the proclamation was the key legal document, "what the treaty said was less important".
</p>


<p>
    In 1932, Governor-General Lord Bledisloe and his wife purchased and presented to the nation the run-down house of James Busby, where the treaty was initially signed. They subsequently donated 500 pounds to restore the building. The Treaty House and grounds were made a public reserve, which was dedicated on 6 February 1934. This event is considered to be the first Waitangi Day.
</p>
<p>
    In 1940, another event was held at the grounds, commemorating the 100th anniversary of the treaty signing. The event was a success and helped raise the profile of the treaty and its day of observance in the national consciousness.
</p>

<h2>
    Annual commemorations
</h2>
<P>
    Annual commemorations of the treaty signing began in 1947. The 1947 event was a Royal New Zealand Navy ceremony centring on a flagpole which the Navy had paid to erect in the grounds. The ceremony was brief and featured no Māori. The following year, a Māori speaker was added to the line-up, and subsequent additions to the ceremony were made nearly every year. From 1952, the Governor-General attended, and from 1958 the Prime Minister also attended, although not every year. From the mid-1950s, a Māori cultural performance was usually given as part of the ceremony. Many of these early features remain a part of Waitangi Day ceremonies, including a naval salute, the Māori cultural performance (now usually a ceremonial welcome), and speeches from a range of Māori and Pākehā dignitaries.
</P>

<h2>
    Proposed as public holiday
</h2>

<p>
    The Labour Party stated in its 1957 election manifesto that it would make Waitangi Day a public holiday. After winning that year''s election, the party said that the country could not afford another public holiday. The Waitangi Day Act of 1960 allowed localities to transfer the holiday from their existing regional public holiday to Waitangi Day.

    In 1963, after a change of government, the passing of the Waitangi Day Amendment Act transferred the holiday observed in Northland on Auckland Anniversary Day (the Monday closest to 29 January) to Waitangi Day, 6 February. This made Waitangi Day a holiday in Northland only.
</p>

<h2>
    Transition to public holiday
</h2>
<p>
    Waitangi Day became a nationwide public holiday on its observance in 1974 by first undergoing a name change. In 1971 the Labour shadow minister of Māori Affairs, Matiu Rata, introduced a private member''s bill to make Waitangi Day a national holiday, to be called New Zealand Day. This was not passed into law.
</p>

<p>
    After the 1972 election of the third Labour government under Prime Minister Norman Kirk, it was announced that from 1974, Waitangi Day would be a national holiday known as New Zealand Day. The New Zealand Day Act legislation was passed in 1973. For Kirk, the change was simply an acceptance that New Zealand was ready to move towards a broader concept of nationhood. Diplomatic posts had for some years marked the day, and it seemed timely in view of the country''s increasing role on the international stage that the national day be known as New Zealand Day. At the 1974 celebrations, the Flag of New Zealand was flown for the first time at the top of the flagstaff at Waitangi, rather than the Union Flag, and a replica of the flag of the United Tribes of New Zealand was also flown.
</p>

<p>
    The election of the third National government in 1975 led to the day being renamed Waitangi Day because the new prime minister, Robert Muldoon, did not like the name "New Zealand Day" and many Māori felt that it debased the treaty. Another Waitangi Day Act was passed in 1976 to change the name back to Waitangi Day and restore Northland''s anniversary day holiday to that of Auckland.
</p>

<p>
    Waitangi Day underwent ''Mondayisation'' in legislation enacted in 2013, shifting the public holiday to Monday if 6 February falls on a Saturday or Sunday.
</p>');

