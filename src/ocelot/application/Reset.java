package ocelot.application;

import java.time.Instant;

public class Reset {
    public String user_id;
    public Instant deadline;

    public Reset(String user_id, Instant deadline) {
        this.user_id = user_id;
        this.deadline = deadline;
    }

    public boolean isExpired() {
        return Instant.now().isAfter(deadline);
    }
}
