package ocelot.application;

public class ArticleNotFoundException extends Exception {

    public ArticleNotFoundException(String id) {
            super("cannot find Article id = " +id);
    }
}
