package ocelot.application;

import java.util.Properties;

public class UnsufficientParameterException extends Exception {
    private Properties inputParameters;
    public UnsufficientParameterException(String message, Properties properties) {
        super(message);
        this.inputParameters = properties;
    }

    public Properties getParameters() {
        return inputParameters;
    }
}
