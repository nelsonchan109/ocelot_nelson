package ocelot.application.servlet;

import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JsonServlet extends CommonServlet {

    protected void sendError(HttpServletRequest req, HttpServletResponse resp,
                           int status, String error) throws ServletException, IOException {

        resp.setStatus(status);
        resp.setContentType("application/json");

        JSONObject jobj = new JSONObject();
        jobj.put("error",error);

        resp.getWriter().println(jobj.toJSONString());

    }
}
