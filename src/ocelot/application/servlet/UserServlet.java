package ocelot.application.servlet;

import ocelot.application.DBConnection;
import ocelot.application.beans.Article;
import ocelot.application.beans.User;
import org.jooq.util.derby.sys.Sys;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class UserServlet extends JsonServlet {


    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        JSONObject jobj = (JSONObject) JSONValue.parse(req.getReader());

        if (jobj == null ) {
            resp.setStatus(400);
            return;
        }

        String method = jobj.get("method").toString();

        try (DBConnection connection = createDBConnection()) {

            switch (method) {
                case "getAllUsers": {
                    int limit = Integer.valueOf(jobj.get("limit").toString());
                    int offset = Integer.valueOf(jobj.get("offset").toString());
                    doGetAllUsers(req, resp, connection, limit, offset);
                    return;
                }

                case "getUserById": {
                    String userId = (String) jobj.get("id");
                    doGetUserById(req, resp, connection, userId);
                    return;
                }

                case "getUserByUserName": {
                    String userName = (String) jobj.get("userName");
                    doGetUserByUserName(req, resp, connection, userName);
                    return;
                }

                case "deleteUserById": {
                    String id = (String) jobj.get("id");
                    System.out.println("deleteUser id :" + id);
                    doDeleteUserById(req, resp, connection, id);
                    return;
                }

                case "deleteUserByUserName": {
                    String name = (String) jobj.get("username");
                    doDeleteUserByUserName(req, resp, connection, name);
                    return;
                }

                case "modifyUser": {

                    User user = User.fromJson(jobj);
                    if (!user.hasValidId()) {
                        sendError(req, resp, 400, "Invalid id");
                        return;
                    }

                    doModifyUser(req, resp, connection, user);
                    return;
                }

                case "addUser": {

                    User user = User.fromJson(jobj);

                    doAddUser(req, resp, connection, user);
                    return;
                }

                case "resetUserPasswd": {

                    String user_id = (String) jobj.get("user_id");
                    String new_pwd = (String) jobj.get("new_pwd");

                    doResetUserPassword(req, resp, connection, user_id, new_pwd);
                    return;
                }
                case "getAvatarList": {

                    String user_id = (String) jobj.get("user_id");

                    doGetAvatarList(req, resp, connection, user_id);
                    return;
                }

                default:
                    sendError(req, resp, 400,  "No such method " + method + "()");
                    break;
            }

        } catch (SQLException e) {
            sendError(req, resp, 400, e.getMessage());
        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
        }
    }


    private void doGetAllUsers(HttpServletRequest req,
                               HttpServletResponse resp,
                               DBConnection connection, int limit, int offset) throws IOException {


        List<User> list = connection.getAllUsers(limit, offset);
        JSONObject jobj = new JSONObject();
        JSONArray articleArray = new JSONArray();



        for (User user : list) {
            JSONObject obj = user.toJson();
            articleArray.add(obj);
        }

        jobj.put("result", articleArray);


        resp.setStatus(200);
        resp.setContentType("application/json");

        resp.getWriter().println(jobj.toJSONString());
    }

    private void doGetUserById(HttpServletRequest req,
                               HttpServletResponse resp,
                               DBConnection connection, String id) throws IOException {


        User user= connection.getUserById(id);
        JSONObject jobj = new JSONObject();

        jobj.put("result", user.toJson());

        resp.setStatus(200);
        resp.setContentType("application/json");

        resp.getWriter().println(jobj.toJSONString());
    }

    private void doGetUserByUserName(HttpServletRequest req,
                               HttpServletResponse resp,
                               DBConnection connection, String userName) throws IOException {


        User user= connection.getUserByUserName(userName);
        JSONObject jobj = new JSONObject();

        jobj.put("result", user == null ? null : user.toJson());

        resp.setStatus(200);
        resp.setContentType("application/json");

        resp.getWriter().println(jobj.toJSONString());
    }

    private void doDeleteUserById(HttpServletRequest req,
                                  HttpServletResponse resp,
                                  DBConnection connection, String id) throws IOException {


        connection.deleteUserById(id);


        resp.setStatus(200);
        resp.setContentType("application/json");

        JSONObject jobj = new JSONObject();
        jobj.put("error", null);

        resp.getWriter().println(jobj.toJSONString());
    }

    private void doDeleteUserByUserName(HttpServletRequest req,
                                  HttpServletResponse resp,
                                  DBConnection connection, String name) throws IOException {


        connection.deleteUserByUserName(name);


        resp.setStatus(200);
        resp.setContentType("application/json");

        JSONObject jobj = new JSONObject();
        jobj.put("error", null);

        resp.getWriter().println(jobj.toJSONString());
    }

    private void doModifyUser(HttpServletRequest req,
                                  HttpServletResponse resp,
                                  DBConnection connection, User user) throws IOException {

        connection.modifyUser(user);

        resp.setStatus(200);
        resp.setContentType("application/json");

        JSONObject jobj = new JSONObject();
        jobj.put("error", null);

        resp.getWriter().println(jobj.toJSONString());

    }

    private void doAddUser(HttpServletRequest req,
                              HttpServletResponse resp,
                              DBConnection connection, User user) throws IOException {

        connection.addUser(user);

        resp.setStatus(200);
        resp.setContentType("application/json");

        JSONObject jobj = new JSONObject();
        jobj.put("error", null);

        resp.getWriter().println(jobj.toJSONString());

    }

    private void doResetUserPassword(HttpServletRequest req,
                                     HttpServletResponse resp,
                                     DBConnection connection,
                                     String user_id, String new_pwd) throws ServletException, IOException {

        User user = connection.getUserById(user_id);
        if (user == null) {
            sendError(req, resp, 400, "No such User");
            return;
        }

        connection.changeUserPwd(user_id, new_pwd);

        System.out.println("new_pwd " + new_pwd );

        resp.setStatus(200);
        resp.setContentType("application/json");

        JSONObject jobj = new JSONObject();
        jobj.put("error", null);

        resp.getWriter().println(jobj.toJSONString());
    }


    private void doGetAvatarList(HttpServletRequest req, HttpServletResponse resp,
                                 DBConnection connection, String user_id) throws ServletException, IOException {

        List<String> list = null;
        if (user_id != null) {
            list = getUserAvatarList(user_id);
        } else {
            list = getDefaultAvtarList();
        }

        JSONObject jobj = new JSONObject();
        JSONArray articleArray = new JSONArray();

        for (String path : list) {
            articleArray.add(path);
        }

        jobj.put("result", articleArray);


        resp.setStatus(200);
        resp.setContentType("application/json");


        resp.getWriter().println(jobj.toJSONString());
    }
}
