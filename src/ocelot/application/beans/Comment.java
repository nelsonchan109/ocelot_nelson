package ocelot.application.beans;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Comment implements Serializable {

    private String id;
    private String userId;
    private String articleId;
    private String parentId;
    private String content;
    private Timestamp pubtime;
    private boolean banned;

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    /* convenient fields from USER table */
    private String userName;
    private String userIcon;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public List<Comment> getChildren() {
        return children;
    }

    public void setChildren(List<Comment> children) {
        this.children = children;
    }

    private List<Comment> children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getPubtime() {
        return pubtime;
    }

    public void setPubtime(Timestamp pubtime) {
        this.pubtime = pubtime;
    }

    public void addChild(Comment comment) {
        children.add(comment);
    }

    public boolean isTopLevelComment() {
        return parentId == null || parentId.isEmpty();
    }

    public Comment(String id, String userId, String articleId, String parentId, String content, Timestamp pubtime,
                   String userName, String userIcon) {
        this.id = id;
        this.userId = userId;
        this.articleId = articleId;
        this.parentId = parentId;
        this.content = content;
        this.pubtime = pubtime;
        this.userName = userName;
        this.userIcon = userIcon;
        children = new ArrayList<>();
    }

    public Comment() {
        children = new ArrayList<>();
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        obj.put("id", getId());
        obj.put("user_id", getUserId());
        obj.put("article_id", getArticleId());
        obj.put("parent_id", getParentId());
        obj.put("content", getContent());
        obj.put("pubtime", getPubtime().getTime());
        obj.put("user_name", getUserName());
        obj.put("user_icon", getUserIcon());
        obj.put("banned", isBanned());

        JSONArray childrenArray = new JSONArray();

        for (Comment child : children) {
            JSONObject o = child.toJson();
            childrenArray.add(o);
        }

        obj.put("children", childrenArray);

        return obj;
    }
}
