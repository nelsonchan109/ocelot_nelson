package ocelot.application.beans;

import org.json.simple.JSONObject;

import java.io.Serializable;
import java.sql.Timestamp;

public class Article implements Serializable {

    private String id;
    private String userId;
    private String title;
    private Timestamp pubtime;
    private String content;
    private int hitcount;
    private int likecount;
    private boolean banned = false;

    //additional field
    private String userName;
    private String userIcon;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public int getHitcount() {
        return hitcount;
    }

    public void setHitcount(int hitcount) {
        this.hitcount = hitcount;
    }

    public int getLikecount() {
        return likecount;
    }

    public void setLikecount(int likecount) {
        this.likecount = likecount;
    }

    public Article() {
    }

    public boolean isValid() {
        return id != null && userId != null;
    }


    public Article(String id, String userId, String title, Timestamp pubtime, String content, int hitcount, int likecount, boolean banned) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.pubtime = pubtime;
        this.content = content;
        this.hitcount = hitcount;
        this.likecount = likecount;
        this.banned = banned;
    }


    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public Timestamp getPubtime() {
        return pubtime;
    }

    public void setPubtime(Timestamp pubtime) {
        this.pubtime = pubtime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public String getEscaptedContent() {

        String result = "";

        for (int i = 0; i < content.length(); i++) {
            if (content.charAt(i) == '\'') {
                result += "\\\'";
            } else {
                result += content.charAt(i);
            }
        }

        return result;
    }
    
    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        obj.put("id", getId());
        obj.put("user_id", getUserId());
        obj.put("title", getTitle());
        obj.put("content", getContent());
        obj.put("pubtime", getPubtime().getTime());
        obj.put("banned", isBanned());
        obj.put("hitcount", getHitcount());
        obj.put("likecount", getLikecount());
        obj.put("userName", getUserName());
        obj.put("userIcon", getUserIcon());

        return obj;
    }
}
