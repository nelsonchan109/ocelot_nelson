package ocelot.application.beans;

import org.json.simple.JSONObject;

import java.io.Serializable;

public class User implements Serializable {
    private String id;
    private String userName;
    private String fname;
    private String lname;
    private String passwd;
    private String gender;
    private String email;
    private String iconName;
    private String dob;
    private String country;
    private String description;
    private boolean verified;

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public User() {
        this.iconName = "default.png";
        this.description = "This guy is lazy!";
    }

    public User(String id, String userName, String fname, String lname, String passwd, String gender, String email, String iconName, boolean verified) {
        this.id = id;
        this.userName = userName;
        this.fname = fname;
        this.lname = lname;
        this.passwd = passwd;
        this.gender = gender;
        this.email = email;
        this.iconName = iconName;
        this.verified = verified;
    }

    public boolean hasValidId() {
        return id != null && id.length() > 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        obj.put("id", getId());
        obj.put("username", getUserName());
        obj.put("fname", getFname());
        obj.put("lname", getLname());
        obj.put("passwd", getPasswd());
        obj.put("gender", getGender());
        obj.put("email", getEmail());
        obj.put("icon", getIconName());
        obj.put("dob", getDob());
        obj.put("country", getCountry());
        obj.put("description", getDescription());
        obj.put("verified", isVerified());

        return obj;
    }

    public static User fromJson(JSONObject jsonObject) {
        User user = new User();

        String id = (String) jsonObject.get("id");
        if (id != null) {
            user.setId(id);
        }

        user.setUserName((String) jsonObject.get("username"));
        user.setFname((String)jsonObject.get("fname"));
        user.setLname((String)jsonObject.get("lname"));
        user.setGender((String)jsonObject.get("gender"));
        user.setPasswd((String)jsonObject.get("passwd"));
        user.setEmail((String)jsonObject.get("email"));
        user.setIconName((String)jsonObject.get("icon"));
        user.setDob((String) jsonObject.get("dob"));
        user.setCountry((String) jsonObject.get("country"));
        user.setDescription((String) jsonObject.get("description"));

        return user;
    }
}
