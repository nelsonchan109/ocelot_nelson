package ocelot.application.beans;

import java.io.Serializable;

public class UserSession implements Serializable {

    private String userId;
    private String userName;

    public UserSession() {

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UserSession(String userId, String userName) {

        this.userId = userId;
        this.userName = userName;
    }
}
